# hex.py

Tests & lints exercises for the Hexlet's Python courses.

## Requirements

All you need is ~~love~~ Docker!

## How to

1. `make`
2. `make register`
3. `hexpy --help` for help
4. PROFIT!

## Scaffolding

`hexpy scaffold foobar` expects that you run it from the "course-python-COURSE" or "challenge-python-COURSE" directory. After that you will have a suitable "python_COURSE_foobar_(exercise|challenge)" subdirectory with Git repo initialized.

## In-repo commands

* `hexpy lint` applies **wemake-python-styleguide** with Hexlet rules,
* `hexpy test`runs **pytest**,
* `hexpy doctest` runs **doctest** for `README.ru.md`,
* `hexpy repo` creates a remote on **BitBucket** (see below).

If you want to use the `repo` command, you should provide a proper credentials: create file `~/.gitspindle` with such content:

```text
[bitbucket]
    user = astynax
    password = ...
```

For password you **shouldn't use your actual password**! You should generate an **application password** instead! Just create one in your profile's settings page.
