#!/usr/bin/env python3

import os
import sys
from functools import wraps

import click
from plumbum import RETCODE, local
from plumbum.cmd import python3, git

DIR = '/usr/src/app'

GIT_SPINDLE_CONFIG_HOME = '/usr'
GIT_SPINDLE_BB = '/linter/lib/bin/git-bb'

COOKIECUTTER = '/linter/lib/bin/cookiecutter'
COOKIECUTTER_CONFIG = '/linter/.cookiecutterrc'
COOKIECUTTER_TEMPLATE = '/linter/template'

PREFIX_COURSE = 'course-python-'
PREFIX_CHALLENGE = 'challenge-python-'


def _try(cmd):
    code = cmd & RETCODE(FG=True)
    if code != 0:
        sys.exit(code)


def _inside_exercise(function):
    """Checks that command runs inside exercise directory."""
    @wraps(function)
    def inner(*args, **kwargs):
        if not os.path.isdir(os.path.join(DIR, 'exercise')):
            click.echo('Run this command from exercise directory!')
            sys.exit(1)
        return function(*args, **kwargs)
    return inner


@click.group()
def cli():
    """Does some stuff with exercises."""
    pass


@cli.command()
@_inside_exercise
def test():
    """Runs pytest for exercise."""
    _try(python3[
        '-m',
        'pytest',
        '--color=yes',
        os.path.join(DIR, 'exercise', 'tests')
    ])


@cli.command()
@_inside_exercise
def doctest():
    """Runs pytest for exercise."""
    _try(python3[
        '-m',
        'doctest',
        os.path.join(DIR, 'README.ru.md')
    ])


@cli.command()
@_inside_exercise
def lint():
    """Runs flake8 for exercise."""
    _try(python3[
        '-m',
        'flake8',
        '--config=/linter/setup.cfg',
        os.path.join(DIR, 'exercise'),
    ])


@cli.command()
@_inside_exercise
def repo():
    """Creates BB repository."""
    assert local.env['HEXPY_DIRNAME']
    if not os.path.isdir(os.path.join(DIR, '.git')):
        click.echo("No Git-repository found here!")
        sys.exit(1)
    with local.env(XDG_CONFIG_HOME=GIT_SPINDLE_CONFIG_HOME):
        with local.cwd(DIR):
            _try(local[GIT_SPINDLE_BB][
                'create',
                '--private',
                '--team=hexlet',
                '--name={}'.format(local.env['HEXPY_DIRNAME']),
            ])


@cli.command()
@click.argument('name')
def scaffold(name):
    """Scaffolds a new exercise."""
    assert local.env['HEXPY_DIRNAME']

    parent = local.env['HEXPY_DIRNAME']
    if parent.startswith(PREFIX_COURSE):
        is_exercise = True
    elif parent.startswith(PREFIX_CHALLENGE):
        is_exercise = False
    else:
        click.echo('Bad CWD. Run me from "(course|challenge)-python-XYZ"!')
        return
    course = parent.replace(
        PREFIX_COURSE, ''
    ).replace(
        PREFIX_CHALLENGE, ''
    ).replace(
        '-', '_'
    )
    name = name.replace('-', '_')
    type_ = 'exercise' if is_exercise else 'challenge'
    target = 'python_{}_{}_{}'.format(course, name, type_)

    _try(local[COOKIECUTTER][
        '--config-file', COOKIECUTTER_CONFIG,
        '-o', DIR,
        '--no-input',
        COOKIECUTTER_TEMPLATE,
        'type={}'.format(type_),
        'name={}'.format(target),
    ])
    with local.cwd(os.path.join(DIR, target)):
        _try(git['init'])


@cli.command()
def script():
    """Dumps script for the target machine."""
    click.echo(r'''
#!/bin/sh

TARGET="$(pwd)"
DIRNAME="$(basename $TARGET)"
if [ -f "$HOME/.gitspindle" ];
then
    SPINDLE_VOLUME="-v $HOME/.gitspindle:/usr/git/spindle:ro"
else
    SPINDLE_VOLUME=""
fi

docker run --rm \
       -u $(id -u):$(id -g) \
       $SPINDLE_VOLUME \
       -v "$TARGET":/usr/src/app:rw \
       -e HEXPY_DIRNAME="$DIRNAME" \
       hexpy:latest \
       $*
'''.strip())


if __name__ == '__main__':
    cli(obj={})
