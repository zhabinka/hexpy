FROM hexlet/common-python-flake8:latest

RUN apk add --no-cache git

WORKDIR /linter

RUN pip install -U --target ./lib \
    pytest plumbum click cookiecutter \
    git+https://github.com/astynax/git-spindle@new-bb-create

ENV PYTHONPATH=/linter/lib:/usr/src/app/exercise/src \
    HEXPY_DIRNAME=""

ADD template ./template/

COPY .cookiecutterrc .

COPY hex.py .
RUN chmod a+x ./hex.py

ENTRYPOINT ["./hex.py"]

CMD ["--help"]
